from ScatBDTXGB import ScatBDTXGB

import pandas as pd

import numpy as np

from skGeo import skGeo

class DirIsoTop(ScatBDTXGB) :
    
    def __init__(self, outFilePath, inFilePath) :
        self.skg = skGeo()
        super(DirIsoTop, self).__init__(name = "DirIsoTop", outFilePath = outFilePath, inFilePath = inFilePath, trainFrac = 0.75)

    def selectiondf(self, df):
        return df.loc[ (df["isct"] == 0) & (df["tubepos[2]"] >= 1800.) ]

    def processdf(self, df):

        # First get tube positions

        df[["tubepos[0]", "tubepos[1]", "tubepos[2]"]] = pd.DataFrame(df["ihPMT"].apply(lambda x : self.skg.pmtPos(x+1)).values.tolist(), index = df.index).astype('float32')
        print "got tubepos"
        # Get zs 
        df["scatvars_zs"] = df["srcpos[2]"].astype('float32')
        print "got zs"
        # Get rs
        df["scatvars_rs"] = ((df["srcpos[0]"]**2 + df["srcpos[1]"]**2)**0.5).astype('float32')
        print "got rs"
        # Get ast
        df["scatvars_ast"] = (np.arctan2( df["tubepos[1]"], df["tubepos[0]"] ) - np.arctan2(df["srcpos[1]"], df["srcpos[0]"])).astype('float32')
        # Fix range
        df["scatvars_ast"] = df["scatvars_ast"].apply( lambda x : x -2*np.pi if x >= np.pi else ( x + 2*np.pi if x < -np.pi else x ))
        print "got ast"
        # Get zt
        df["scatvars_zt"] = df["tubepos[2]"].astype('float32')
        print "got zt"
        # Get rt
        df["scatvars_rt"] = ((df["tubepos[0]"]**2 + df["tubepos[1]"]**2)**0.5).astype('float32')
        print "got rt"
        # Randomize direction of direct light
        df[["srcdir_new[0]", "srcdir_new[1]", "srcdir_new[2]"]] = pd.DataFrame( self.sample_spherical(len(df)), index = df.index).astype('float32')
        dirIndex = df['isct'] == 0
        df.loc[dirIndex, ["srcdir[0]", "srcdir[1]", "srcdir[2]"] ] = (df.loc[dirIndex, ["srcdir_new[0]", "srcdir_new[1]", "srcdir_new[2]"]].values).astype('float32')
        print "got randomized directions for direct light"
        # Get ct
        df["scatvars_ct"] = df["srcdir[2]"].astype('float32')
        print "got ct"        
        # Get phi
        df["scatvars_phi"] = (np.arctan2(df["srcdir[1]"], df["srcdir[0]"] ) - np.arctan2(df["tubepos[1]"]-df["srcpos[1]"], df["tubepos[0]"]-df["srcpos[0]"] )).astype('float32')
        # Fix range
        df["scatvars_phi"] = df["scatvars_phi"].apply( lambda x : x -2*np.pi if x >= np.pi else ( x + 2*np.pi if x < -np.pi else x ))
        print "got phi"

        return df

    def cleandf(self, df) :
        return df.drop(labels = ["srcpos[0]", "srcpos[1]", "srcpos[2]", "srcdir[0]", "srcdir[1]", "srcdir[2]", "srcdir_new[0]", "srcdir_new[1]", "srcdir_new[2]", "ihPMT", "tubepos[0]", "tubepos[1]", "tubepos[2]", "isct", "scatvars_zt"], axis = 1)

    # Variables to be used in training
    observables = { "scatvars_zs"       : { "label" : r'Source Z [cm]',                                      "range" : [-1800, 1800] , "logScale" : False },
                    "scatvars_rs"       : { "label" : r'Source R [cm]}',                                     "range" : [0., 1800.] , "logScale"   : False },
                    "scatvars_rt"       : { "label" : r'Tube R [cm]',                                        "range" : [0., 1800.] , "logScale"   : False },
                    "scatvars_ast"      : { "label" : r'Azimuthal angle between source and tube positions',  "range" : [-np.pi, np.pi] , "logScale"     : False },
                    "scatvars_ct"       : { "label" : r'cosine(#theta_{sourcedir})',                         "range" : [-1., 1.] , "logScale"     : False },
                    "scatvars_phi"      : { "label" : r'Azimuthal angle between source direction and vector between source and tube positions', "range" : [-np.pi, np.pi] , "logScale"     : False }
    }


    # Generate random vector on the unit sphere
    def sample_spherical(self, n = 1):
        vec = np.random.randn(n, 3)
        vec /= np.linalg.norm(vec, axis=1)[:,None]
        return vec

    def rawVariables(self) :
        return ["isct",
                "ihPMT",
                "srcpos",
                "srcdir"]

class IndirTop(DirIsoTop) :
    def __init__(self, outFilePath, inFilePath) :
        self.skg = skGeo()
        super(DirIsoTop, self).__init__(name = "IndirTop", outFilePath = outFilePath, inFilePath = inFilePath, trainFrac = 0.75)

    # Override selection
    def selectiondf(self, df):
        return df.loc[ (df["isct"] != 0) & (df["tubepos[2]"] >= 1800.) ]


class DirIsoBot(DirIsoTop) :
    def __init__(self, outFilePath, inFilePath) :
        self.skg = skGeo()
        super(DirIsoTop, self).__init__(name = "DirIsoBot", outFilePath = outFilePath, inFilePath = inFilePath, trainFrac = 0.75)

    # Override selection
    def selectiondf(self, df):
        return df.loc[ (df["isct"] == 0) & (df["tubepos[2]"] <= -1800.) ]

    
class IndirBot(DirIsoTop) :
    def __init__(self, outFilePath, inFilePath) :
        self.skg = skGeo()
        super(DirIsoTop, self).__init__(name = "IndirBot", outFilePath = outFilePath, inFilePath = inFilePath, trainFrac = 0.75)

    # Override selection
    def selectiondf(self, df):
        return df.loc[ (df["isct"] != 0) & (df["tubepos[2]"] <= -1800.) ]


class DirIsoBar(DirIsoTop) :
    def __init__(self, outFilePath, inFilePath) :
        self.skg = skGeo()
        super(DirIsoTop, self).__init__(name = "DirIsoBar", outFilePath = outFilePath, inFilePath = inFilePath, trainFrac = 0.75)

    # Override selection
    def selectiondf(self, df):
        return df.loc[ (df["isct"] == 0) & (df["tubepos[2]"] <= 1800.) & (df["tubepos[2]"] >= -1800.) ]


    observables = { "scatvars_zs"       : { "label" : r'Source Z [cm]',                                      "range" : [-1800, 1800] , "logScale" : False },
                    "scatvars_rs"       : { "label" : r'Source R [cm]}',                                     "range" : [0., 1800.] , "logScale"   : False },
                    "scatvars_zt"       : { "label" : r'Tube Z [cm]',                                        "range" : [-1800., 1800.] , "logScale"   : False },
                    "scatvars_ast"      : { "label" : r'Azimuthal angle between source and tube positions',  "range" : [-np.pi, np.pi] , "logScale"     : False },
                    "scatvars_ct"       : { "label" : r'cosine(#theta_{sourcedir})',                         "range" : [-1., 1.] , "logScale"     : False },
                    "scatvars_phi"      : { "label" : r'Azimuthal angle between source direction and vector between source and tube positions', "range" : [-np.pi, np.pi] , "logScale"     : False }
    }

    def cleandf(self, df) :
        return df.drop(labels = ["srcpos[0]", "srcpos[1]", "srcpos[2]", "srcdir[0]", "srcdir[1]", "srcdir[2]", "srcdir_new[0]", "srcdir_new[1]", "srcdir_new[2]", "ihPMT", "tubepos[0]", "tubepos[1]", "tubepos[2]", "isct", "scatvars_rt"], axis = 1)


class IndirBar(DirIsoBar) :
    def __init__(self, outFilePath, inFilePath) :
        self.skg = skGeo()
        super(DirIsoTop, self).__init__(name = "IndirBar", outFilePath = outFilePath, inFilePath = inFilePath, trainFrac = 0.75)

    # Override selection
    def selectiondf(self, df):
        return df.loc[ (df["isct"] != 0) & (df["tubepos[2]"] < 1800.) & (df["tubepos[2]"] > -1800.) ]
