from ScatBDTBase import ScatBDTBase

import pandas as pd

import os

import xgboost as xgb

import treelite

import pickle

import time

class ScatBDTXGB(ScatBDTBase) :

    def __init__(self, name, outFilePath, inFilePath, trainFrac = 0.75):
        super(ScatBDTXGB, self).__init__(name = name,  outFilePath = outFilePath, inFilePath = inFilePath, trainFrac = 0.75)

    def xgbPath(self) :
        return self.baseDir()+self.name+"_xgb.model"

    def treelitePath(self) :
        return self.baseDir()+self.name+".zip"

    def treeliteAnnotationPath(self) :
        return self.baseDir()+self.name+"_annotation.json"

    def trainBDT(self, targetSample) :

        originDF, originDFtest = self.getDataFrames()
        targetDF, targetDFtest = targetSample.getDataFrames()

        if self.observables.keys() != targetSample.observables.keys() :
            print 'Error observables for target and origin data sets do not match. Exiting...'
            print 'Origin:', self.observables.keys()
            print 'Target:', targetSample.observables.keys()
            exit(-1)
        
        originDF = originDF[self.observables.keys()]
        targetDF = targetDF[targetSample.observables.keys()]

        labelOrigin = [1]*len(originDF)
        labelTarget = [0]*len(targetDF)

        originDFtest = originDFtest[self.observables.keys()]
        targetDFtest = targetDFtest[targetSample.observables.keys()]

        labelOrigintest = [1]*len(originDFtest)
        labelTargettest = [0]*len(targetDFtest)
        
        allDF = pd.concat([originDF, targetDF])
        label = labelOrigin + labelTarget
        
        allDFtest = pd.concat([originDFtest, targetDFtest])
        labeltest = labelOrigintest + labelTargettest

        data = xgb.DMatrix(data = allDF, label = label)

        datatest = xgb.DMatrix(data = allDFtest, label = labeltest)

        params = {}
        params['nthread'] = 1
        params["tree_method"] = "gpu_hist"
        #params["tree_method"] = "auto" 
        params["objective"] = 'reg:logistic'
#        params["eval_metric"] = 'logloss'
        params["eta"] = 1.0
        params["max_depth"] = 3

        eval_result = {}
        evals = [(data, "train"), (datatest, "test")]

        bst = xgb.train(params = params, dtrain = data,  num_boost_round=200, evals = evals, evals_result = eval_result , verbose_eval = 10)
        
        for f in [ self.xgbPath()+'.raw.txt', self.xgbPath()+'.eval.p', self.xgbPath() ] :
            try:
                os.remove(f)
            except OSError:
                pass

        # dump model
        bst.dump_model(self.xgbPath()+'.raw.txt')
        with open(self.xgbPath()+'.eval.p', "w") as f :
            pickle.dump(eval_result, f)
        
        bst.save_model(self.xgbPath())
        print "Saved XGBoost model"


    def exportTreeLite(self, targetSample) :
#        originDF, originDFtest = self.getDataFrames()
#        targetDF, targetDFtest = targetSample.getDataFrames()
#        if self.observables.keys() != targetSample.observables.keys() :
#            print 'Error observables for target and origin data sets do not match. Exiting...'
#            print 'Origin:', self.observables.keys()
#            print 'Target:', targetSample.observables.keys()
#            exit(-1)
#        
#        originDF = originDF[self.observables.keys()]
#        targetDF = targetDF[targetSample.observables.keys()]
#
#        labelOrigin = [1]*len(originDF)
#        labelTarget = [0]*len(targetDF)
#
#        originDFtest = originDFtest[self.observables.keys()]
#        targetDFtest = targetDFtest[targetSample.observables.keys()]
#
#        labelOrigintest = [1]*len(originDFtest)
#        labelTargettest = [0]*len(targetDFtest)
#        
#        allDF = pd.concat([originDF, targetDF])
#        label = labelOrigin + labelTarget
#        
#        allDFtest = pd.concat([originDFtest, targetDFtest])
#        labeltest = labelOrigintest + labelTargettest
#
#        data = xgb.DMatrix(data = allDF, label = label)
#
#        datatest = xgb.DMatrix(data = allDFtest, label = labeltest)


        bst = xgb.Booster({'nthread': 1})  # init model
        bst.load_model(self.xgbPath())  # load data

        treeliteModel = treelite.Model.from_xgboost(bst)
        print "Got treelite Model"

#        annotator = treelite.Annotator()
        # Annotate branches by iterating over the training data

#        annotator.annotate_branch(model=treeliteModel, dmat=treelite.DMatrix(allDF), verbose=True)
#        print "Annotated treelite model"
#        annotator.save(path=self.treeliteAnnotationPath())
#        print "Saved annotation"

        treeliteModel.export_srcpkg(platform='unix', toolchain='gcc',
                                    pkgpath=self.treelitePath(), libname=self.name+'.so',
                                    verbose=True, params={"quantize" : 1}) #, params={'annotate_in': self.treeliteAnnotationPath()})
        print "Exported treelite model"

    def plotDiagnostics(self, targetSample, weightScheme = "xgb") :
        print self.name, "Plotting diagnostics"
        if not os.path.isdir(self.plotsDir()) :
            os.makedirs(self.plotsDir())

        targetDFtrain, targetDFtest = targetSample.getDataFrames()
        originDFtrain, originDFtest = self.getDataFrames()

        targetDFtestObs = targetDFtest[self.observables.keys()]
        originDFtestObs = originDFtest[self.observables.keys()]

        bst = xgb.Booster(model_file=self.xgbPath())
        
        print bst

        testData = xgb.DMatrix(originDFtestObs)

        print self.name, "Number of points to calculate", len(originDFtestObs)
        start = time.time()
#        weightsTest = [ 1/x-1 for x in bst.predict(testData) ]
        predsTest = bst.predict(testData)
        weightsTest = 1/predsTest - 1
        
        end = time.time()
        print self.name, "Time elapsed", (end-start)

        figTarget = self.getCornerPlot(dataFrame = targetDFtestObs, color = 'r', label = 'Nominal' )
        figTarget.savefig(self.plotsDir()+"corner_"+self.name+"_targetOnly.png", transparent = True, figsize = {50, 50}, dpi = 240)

        figOrigin = self.getCornerPlot(dataFrame = originDFtestObs, color = 'b', label = self.name, fig = figTarget )
        figTarget.savefig(self.plotsDir()+"corner_"+self.name+"_target_originNotRW.png", transparent = True, figsize = {50, 50}, dpi = 240)

        figOriginRW = self.getCornerPlot(dataFrame = originDFtestObs, color = 'g', label = self.name , weights = weightsTest, fig = figTarget)
        figTarget.savefig(self.plotsDir()+"corner_"+self.name+"_"+weightScheme+".png", transparent = True, figsize = {50, 50}, dpi = 240)

