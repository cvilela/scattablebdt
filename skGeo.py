#!/usr/bin/python

import pandas as pd
import numpy as np
from collections import OrderedDict

class skGeo(object) :
    def __init__(self) :
        self.sk4GeoFile = "connection.super.sk-4.dat"
        self.columns = OrderedDict()
        self.columns["icab" ] = np.int32
        self.columns["pmtSuperModuleAddress" ] = str
        self.columns["pmtSuperModuleSubAddress"] = str
        self.columns["pmtSuperModuleSerialNumber"] = str
        self.columns["pmtModuleSerialNumber"] = str
        self.columns["hutNumber"] = str
        self.columns["TKOBOXnumber"] = str
        self.columns["TKOModuleAddress"] = str
        self.columns["QBChannel"] = str
        self.columns["IDHVCrate_or_ODHutNumber"] = str
        self.columns["HVModuleAddress"] = str
        self.columns["HVChannel"] = str
        self.columns["HVValue"] = str
        self.columns["PMTSerialNumber"] = str
        self.columns["PMTFlag"] = str
        self.columns["QBIPAddress"] = str
        self.columns["PMT_x" ] = np.float64
        self.columns["PMT_y" ] = np.float64
        self.columns["PMT_z" ] = np.float64
        self.columns["ODPaddleCardNumber_hut"] = str
        self.columns["ODPaddleCardNumber_crate"] = str
        self.columns["ODPaddleCardNumber_module"] = str
        self.columns["ODPaddleCardNumber_channel"] = str

        self.df = pd.read_csv(self.sk4GeoFile, header=None, delim_whitespace = True, skiprows = 48, names = self.columns.keys())

        self.pmtPosMap = self.df[["icab", "PMT_x", "PMT_y", "PMT_z"]]

        self.pmtPosDict = self.pmtPosMap.set_index('icab').T.to_dict('list')

    def pmtPos(self, icab) :
        try :
            return self.pmtPosDict[icab]
        except KeyError, e :
            print "skGeo.pmtPos got key error, icab =", icab
            print self.pmtPosDict
            exit(-1)
