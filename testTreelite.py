import treelite
import pandas as pd

sample = "Bot"
annotate = False


modelPath = "/dune/data/users/cvilela/MagicRW/FHC_RecoLepE_RecoY_200Trees/ProtonEdepm20pc/ProtonEdepm20pc_trueKinBDT.xgb"
outPath = "/dune/data/users/cvilela/MagicRW/FHC_RecoLepE_RecoY_200Trees/ProtonEdepm20pc/"

model = treelite.Model.load(modelPath, model_format='xgboost')

model.export_srcpkg(platform='unix', toolchain='gcc',
                    pkgpath=outPath+'/bdt'+sample+'.zip', libname='bdt'+sample+'.so',
                    verbose=True) #, params = {"native_lib_name" : "aname", "annotate_in" : "mymodel-annotation.json"} )
