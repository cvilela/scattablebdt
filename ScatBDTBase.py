from random import random
import pandas as pd
import cPickle as pickle
import os
from corner import corner
import matplotlib.pyplot as plt
import numpy as np
import uproot
import glob

import time

class ScatBDTBase(object) :
    
    def __init__(self, name, outFilePath, inFilePath, trainFrac = 0.0):
        self.name = name
        self.outFilePath = outFilePath
        self.inFilePath = inFilePath
        self.trainFrac = trainFrac
        super(ScatBDTBase, self).__init__()
                
    def observables() :
        pass

    def dataframe(self) :
        
        dfs = []

        print self.rawVariables()

        files = glob.glob(self.inFilePath)

        for f in files :
            print "Starting"
            print f
            sttree = uproot.open(f)['sttree']
            sttree = sttree.arrays(["isct", "ihPMT", "srcpos", "srcdir"] ,outputtype=pd.DataFrame)
            print "Got Array"

            # Process variables
            sttree = self.processdf(sttree)
            print "Processed Array"
            print sttree

            # Apply selection
            sttree = self.selectiondf(sttree)
            print "Selected Array"
            print sttree
 
            # Remove temporaries
            sttree = self.cleandf(sttree)
            print "Cleaned Array"
            print sttree

            dfs.append(sttree)
    
        df = pd.concat(dfs)
        print "Merged DataFrames"

        if (self.trainFrac == 0.0) or (self.trainFrac == 1.0) :
            return df
        else :

            mask = np.array([[ x <= self.trainFrac, x > self.trainFrac] for x in np.random.rand(len(df)) ]).T
            df_train = df.loc[mask[0]]
            print "Got train sample"

            df_test = df.loc[mask[1]]
            print "Got test sample"
            
            return df_train, df_test

    def baseDir(self) :
        return self.outFilePath+"/"+self.name+"/"

    def plotsDir(self) :
        return self.outFilePath+"/"+self.name+"/Plots/"

    def testDFpath(self) :
        return self.baseDir()+self.name+"_test.p"
    
    def trainDFpath(self) :
        return self.baseDir()+self.name+"_train.p"

    def gbrwPath(self) :
        return self.baseDir()+self.name+"_gbrw.p"
        
    def pickleData(self) :
        trainSet, testSet = self.dataframe()

        if not os.path.isdir(self.baseDir()) :
            os.makedirs(self.baseDir())
        
        with open(self.trainDFpath(), "wb")  as f :
            pickle.dump(trainSet, f, protocol = pickle.HIGHEST_PROTOCOL)
        
        with open(self.testDFpath(), "wb")  as f :
            pickle.dump(testSet, f, protocol = pickle.HIGHEST_PROTOCOL)

        return trainSet, testSet

    def getDataFrames(self, train = True, test = True) :

        try :
            if train :
                fTrain = open(self.trainDFpath(), 'r')
                trainDF = pickle.load(fTrain)
            if test :
                fTest = open(self.testDFpath(), 'r')
                testDF = pickle.load(fTest)
        except IOError as e :
            print 'MagicRWsample getTrainDF I/O error({0}): {1}'.format(e.errno, e.strerror), 'Attempting to produce dataframes...'
            trainDF, testDF = self.pickleData()
            print 'New dataframes produced'
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
        finally :
            if train and test :
                return trainDF, testDF
            elif train :
                return trainDF
            elif test :
                return testDF

    def getTrainDataFrame(self) :
        return self.getDataFrames(train = True, test = False)

    def getTestDataFrame(self) :
        return self.getDataFrames(train = False, test = True)

    def plotDiagnostics(self, targetSample, weightScheme = "gbrw") :
        print self.name, "Plotting diagnostics"
        if not os.path.isdir(self.plotsDir()) :
            os.makedirs(self.plotsDir())

        targetDFtrain, targetDFtest = targetSample.getDataFrames()
        originDFtrain, originDFtest = self.getDataFrames()

        targetDFtestObs = targetDFtest[self.observables.keys()]
        originDFtestObs = originDFtest[self.observables.keys()]

        with open(self.gbrwPath(), 'r') as f :
            reweighter = pickle.load(f)
            
            print self.name, "Number of points to calculate", len(originDFtestObs)
            start = time.time()
            weightsTest  = reweighter.predict_weights(originDFtestObs)
            end = time.time()
            print self.name, "Time elapsed", (end-start)

        figTarget = self.getCornerPlot(dataFrame = targetDFtestObs, color = 'r', label = 'Nominal' )
        figTarget.savefig(self.plotsDir()+"corner_"+self.name+"_targetOnly.png", transparent = True, figsize = {50, 50}, dpi = 240)

        figOrigin = self.getCornerPlot(dataFrame = originDFtestObs, color = 'b', label = self.name, fig = figTarget )
        figTarget.savefig(self.plotsDir()+"corner_"+self.name+"_target_originNotRW.png", transparent = True, figsize = {50, 50}, dpi = 240)

        figOriginRW = self.getCornerPlot(dataFrame = originDFtestObs, color = 'g', label = self.name , weights = weightsTest, fig = figTarget)
        figTarget.savefig(self.plotsDir()+"corner_"+self.name+"_"+weightScheme+".png", transparent = True, figsize = {50, 50}, dpi = 240)

    def getCornerPlot(self, dataFrame, color, label, weights = None, fig = None) :

        labels    = [ observable["label"]    for key, observable in self.observables.items() ]
        ranges    = [ observable["range"]    for key, observable in self.observables.items() ]
        logScales = [ observable["logScale"] for key, observable in self.observables.items() ]

        histArgs1d = {"normed" : True , "linewidth" : 1.5}
        contourArgs = { "linewidths" : 1.5}

        bins = 50

        fig =  corner(dataFrame, labels = labels, plot_contours = True, plot_datapoints = False, plot_density = False, color = color, bins = bins, max_n_ticks = 6, weights = weights, range = ranges, no_fill_contours = True, fig = fig, hist_kwargs = dict(histArgs1d.items() + {"color" : color}.items()+{"label" : label}.items()), contour_kwargs = dict(contourArgs.items() + {"color" : color}.items()), smooth = True, smooth1d = None, label = label )

        for i in range(0, len(logScales)) :
            if logScales[i] :
                fig.axes[i*len(logScales)+i].set_yscale("log")
                fig.axes[i*len(logScales)+i].set_ylim(ymin = 1)
                fig.axes[i*len(logScales)+i].yaxis.set_label_position("right")

        return fig

