from multiprocessing import Process

from ScatBDTSamples import *

pickle = True
train = True
exportTreeLite = True
produceBinned = False

plot = False
plotBinned = False

inPath =  "/storage/shared/cvilela/SKDETSIM_scattab/11_0_[0-4]_sttree.root"
outPath = "/storage/shared/cvilela/scatBDTSKDETSIM_uproot_5/"

Top = [ IndirTop   ( inFilePath = inPath, outFilePath = outPath ),
        DirIsoTop  ( inFilePath = inPath, outFilePath = outPath )]
Bot = [ IndirBot   ( inFilePath = inPath, outFilePath = outPath ),
        DirIsoBot  ( inFilePath = inPath, outFilePath = outPath )]
Bar = [ IndirBar   ( inFilePath = inPath, outFilePath = outPath ),
        DirIsoBar  ( inFilePath = inPath, outFilePath = outPath )]

samples = [Top, Bot, Bar ]
#samples = [ Bar ]

if pickle :
    processesPickle = []
    for sample in samples :
        for s in sample : 
            processesPickle.append( Process( target = s.pickleData ) )

    for p in processesPickle :
        p.start()
    for p in processesPickle :
        p.join()

if train :
    processesPickle = []

    for sample in samples :
        sNom = sample[0]
        for s in sample[1:] :
            processesPickle.append( Process( target = s.trainBDT, args=(sNom,) ) )
        
    for p in processesPickle :
        p.start()
    for p in processesPickle :
        p.join()

if exportTreeLite :
    processesPickle = []

    for sample in samples :
        sNom = sample[0]
        for s in sample[1:] :
            processesPickle.append( Process( target = s.exportTreeLite, args=(sNom,) ) )
        
    for p in processesPickle :
        p.start()
    for p in processesPickle :
        p.join()

if plot :
    processesPickle = []

    for sample in samples :
        sNom = sample[0]
        for s in sample[1:] :
            processesPickle.append( Process( target = s.plotDiagnostics, args=(sNom,) ) )

    for p in processesPickle :
        p.start()
    for p in processesPickle :
        p.join()

if produceBinned :
    processesPickle = []

    for sample in samples :
        sNom = sample[0]
        for s in sample[1:] :
            processesPickle.append( Process( target = s.makeBinnedWeights ) )

    for p in processesPickle :
        p.start()
    for p in processesPickle :
        p.join()

#if plotBinned :
#    processesPickle = []
#
#    for sample in samples :
#        sNom = sample[0]
#        for s in sample[1:] :
#            processesPickle.append( Process( target = s.plotDiagnostics, args=(sNom, "EnuTp",) ) )
#            processesPickle.append( Process( target = s.plotDiagnostics, args=(sNom, "ElTp",) ) )
#            processesPickle.append( Process( target = s.plotDiagnostics, args=(sNom, "q0q3",) ) )
#            processesPickle.append( Process( target = s.plotDiagnostics, args=(sNom, "EnuW",) ) )
#            processesPickle.append( Process( target = s.plotDiagnostics, args=(sNom, "EnuQ2",) ) )
#
#    for p in processesPickle :
#        p.start()
#    for p in processesPickle :
#        p.join()
#
